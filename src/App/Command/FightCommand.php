<?php

namespace App\Command;

use App\Model\Beast;
use App\Model\Factory\BeastCreator;
use App\Model\Factory\FightCreator;
use App\Model\Factory\HeroCreator;
use App\Model\Hero;

require __DIR__.'/../../../vendor/autoload.php';

class FightCommand {

    public function fight()
    {
        $beastCreator = new BeastCreator();
        $beast = $beastCreator->createFighter();

        $heroCreator = new HeroCreator();
        $hero = $heroCreator->createFighter();

        $fightCreator = new FightCreator();
        $fight = $fightCreator->createFight();

        $fight->determineFirstAttacker($hero, $beast);
        $fight->determineDefender($hero, $beast);

        echo $this->getFighterStats($fight->getAttacker());

        echo $this->getFighterStats($fight->getDefender());

        while($fight->fightIsOn()) {
            echo "\n ===== Attack ===== \n";
            $testChanceNumber = rand(1, 100);
            echo "Lucky Chance: " . $testChanceNumber; echo "\n";
            echo "Turn: " . $fight->getTurns() . "\n";
            echo "Defender health before attack: " . $fight->getDefender()->getFightStats()->getHealth() . "\n";
            echo "Attacker: " . $fight->getAttacker()->getName() . " vs. Defender: " . $fight->getDefender()->getName() . ".\n";

            if(!$fight->isLuckyDefender($testChanceNumber)) {
                $damage = $fight->calculateDamage();
                echo "Standard damage: " . $damage . ".\n";

                $damage = $fight->applyFightersSkills($fight->getAttacker(), $testChanceNumber, $damage);

                $damage = $fight->applyFightersSkills($fight->getDefender(), $testChanceNumber, $damage);

                $fight->applyDamage($damage);
            }
            else {
                echo "Damage: 0 (Lucky Defender). \n";
            }
            echo "\nDefender Health " . $fight->getDefender()->getFightStats()->getHealth() . "\n";

            if($fight->fightIsOn()) {
                $fight->changeFightRoles();
                $fight->setTurns($fight->getTurns() + 1);
                echo "\n";
            }
            else {
                echo "\n !!! WINNER: " . $fight->getAttacker()->getName() . " !!! \n\n";
            }
        }
    }

    /**
     * @param @param Hero|Beast $fighter
     * @return string
     */
    public function getFighterStats($fighter)
    {
        return "\nStats: \n" . $fighter->getName() .
            "\n - Health: " . $fighter->getFightStats()->getHealth() .
            "\n - Strength: " . $fighter->getFightStats()->getStrength() .
            "\n - Defense: " . $fighter->getFightStats()->getDefence() .
            "\n - Speed: " . $fighter->getFightStats()->getSpeed() .
            "\n - Luck: " . $fighter->getFightStats()->getLuck() . "% \n";
    }
}

$obj = new FightCommand();
$obj->fight();