<?php


namespace App\Model\Skills;


class MagicShield implements Skill
{
    private string $name = 'Magic Shield';

    private int $chance = 20;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getChance()
    {
        return $this->chance;
    }

    public function damageTransform($damage)
    {
        return $damage / 2;
    }
}