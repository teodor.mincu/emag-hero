<?php


namespace App\Model\Skills;


interface Skill
{
    public function getName();

    public function getChance();

    public function damageTransform($damage);
}