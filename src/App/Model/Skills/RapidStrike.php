<?php


namespace App\Model\Skills;


class RapidStrike implements Skill
{
    private string $name = 'Rapid Strike';

    private int $chance = 10;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getChance()
    {
        return $this->chance;
    }

    public function damageTransform($damage)
    {
        return 2 * $damage;
    }
}