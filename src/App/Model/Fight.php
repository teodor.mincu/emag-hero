<?php

namespace App\Model;

class Fight
{
    const MAX_TURNS = 20;

    private $attacker;

    /** @var Hero|Beast|null */
    private $defender;

    /** @var int */
    private $turns;

    /**
     * @return Hero|Beast|null
     */
    public function getAttacker()
    {
        return $this->attacker;
    }

    /**
     * @param mixed $attacker
     * @return Fight
     */
    public function setAttacker($attacker)
    {
        $this->attacker = $attacker;
        return $this;
    }

    /**
     * @return Beast|Hero|null
     */
    public function getDefender()
    {
        return $this->defender;
    }

    /**
     * @param $defender
     * @return $this
     */
    public function setDefender($defender)
    {
        $this->defender = $defender;
        return $this;
    }

    /**
     * @return int
     */
    public function getTurns(): int
    {
        return $this->turns;
    }

    /**
     * @param int $turns
     * @return Fight
     */
    public function setTurns(int $turns): Fight
    {
        $this->turns = $turns;
        return $this;
    }

    public function determineFirstAttacker(Hero $hero, Beast $beast)
    {
        /** If speed and luck are both equals, the good has the first chance :) */
        $this->setAttacker($hero);

        if($hero->getFightStats()->getSpeed() !== $beast->getFightStats()->getSpeed()) {
            $hero->getFightStats()->getSpeed() > $beast->getFightStats()->getSpeed() ? $this->setAttacker($hero) : $this->setAttacker($beast);
            return $this;
        }

        if($hero->getFightStats()->getLuck() !== $beast->getFightStats()->getLuck()) {
            $hero->getFightStats()->getLuck() > $beast->getFightStats()->getLuck() ? $this->setAttacker($hero) : $this->setAttacker($beast);
            return $this;
        }

        return $this;
    }

    public function determineDefender(Hero $hero, Beast $beast)
    {
        if($this->getAttacker() instanceof Hero) {
            $this->setDefender($beast);
            return $beast;
        }

        if($this->getAttacker() instanceof Beast) {
            $this->setDefender($hero);
            return $hero;
        }

        return false;
    }

    public function calculateDamage()
    {
        return $this->getAttacker()->getFightStats()->getStrength() - $this->getDefender()->getFightStats()->getDefence();
    }

    public function applyDamage($damage)
    {
        $this->getDefender()->getFightStats()->setHealth($this->getDefender()->getFightStats()->getHealth() -  $damage);
        return $this;
    }

    public function fightIsOn()
    {
        if($this->getTurns() == Fight::MAX_TURNS) {
            return false;
        }

        if($this->getDefender()->getFightStats()->getHealth() <= 0) {
            return false;
        }

        return true;
    }

    public function isLuckyDefender($testChanceNumber)
    {
        if($testChanceNumber < $this->getDefender()->getFightStats()->getLuck()) {
            return true;
        }
        return false;
    }

    public function changeFightRoles()
    {
        $attacker = $this->getAttacker();
        $this->setAttacker($this->getDefender());
        $this->setDefender($attacker);
        return $this;
    }

    /**
     * @param Hero|Beast s$fighter
     * @param $testChanceNumber
     * @param $damage
     * @return mixed
     */
    public function applyFightersSkills($fighter, $testChanceNumber, $damage)
    {
        foreach ($fighter->getSkills() as $skill) {
            if($testChanceNumber < $skill->getChance()) {
                $damage = $skill->damageTransform($damage);
                echo " *** Skill occurred by chance: " . $skill->getName() . " *** Damage: " . $damage . "\n";
            }
        }
        return $damage;
    }
}