<?php


namespace App\Model\Factory;


interface FighterCreator
{
    public function createFighter();

    public function createFightStats();
}