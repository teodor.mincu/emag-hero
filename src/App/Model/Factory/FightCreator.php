<?php

namespace App\Model\Factory;

use App\Model\Fight;

class FightCreator
{
    public function createFight()
    {
        $fight = new Fight();
        $fight
            ->setTurns(1);

        return $fight;
    }
}