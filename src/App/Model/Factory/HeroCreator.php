<?php


namespace App\Model\Factory;


use App\Model\FightStats;
use App\Model\Hero;
use App\Model\Skills;

class HeroCreator implements FighterCreator
{
    /**
     * @return FightStats
     */
    public function createFightStats()
    {
        $fightStats = new FightStats();
        $fightStats
            ->setHealth(rand(Hero::HEALTH_MIN, Hero::HEALTH_MAX))
            ->setStrength(rand(Hero::STRENGTH_MIN, Hero::STRENGTH_MAX))
            ->setDefence(rand(Hero::DEFENCE_MIN, Hero::DEFENCE_MAX))
            ->setSpeed(rand(Hero::SPEED_MIN, Hero::SPEED_MAX))
            ->setLuck(rand(Hero::LUCK_MIN, Hero::LUCK_MAX));
        return $fightStats;
    }

    public function createFighter(): Hero
    {
        $hero = new Hero();
        $hero
            ->setFightStats($this->createFightStats())
            ->setSkills([
                new Skills\RapidStrike(),
                new Skills\MagicShield()
            ])
            ->setName(Hero::NAME);

        return $hero;
    }
}