<?php

namespace App\Model\Factory;

use App\Model\Beast;
use App\Model\FightStats;

class BeastCreator implements FighterCreator
{
    /**
     * @return FightStats
     */
    public function createFightStats()
    {
        $fightStats = new FightStats();
        $fightStats
            ->setHealth(rand(Beast::HEALTH_MIN, Beast::HEALTH_MAX))
            ->setStrength(rand(Beast::STRENGTH_MIN, Beast::STRENGTH_MAX))
            ->setDefence(rand(Beast::DEFENCE_MIN, Beast::DEFENCE_MAX))
            ->setSpeed(rand(Beast::SPEED_MIN, Beast::SPEED_MAX))
            ->setLuck(rand(Beast::LUCK_MIN, Beast::LUCK_MAX));

        return $fightStats;
    }

    /**
     * @return Beast
     */
    public function createFighter(): Beast
    {
        $beast = new Beast();
        $beast
            ->setFightStats($this->createFightStats())
            ->setSkills([])
            ->setName(Beast::NAME);

        return $beast;
    }
}