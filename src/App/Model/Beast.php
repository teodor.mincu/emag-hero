<?php

namespace App\Model;

use App\Model\Skills\Skill;

class Beast
{
    const NAME = 'Wild Beast';

    const HEALTH_MIN = 60;
    const HEALTH_MAX= 90;

    const STRENGTH_MIN = 60;
    const STRENGTH_MAX= 90;

    const DEFENCE_MIN = 40;
    const DEFENCE_MAX= 60;

    const SPEED_MIN = 40;
    const SPEED_MAX= 60;

    const LUCK_MIN = 25;
    const LUCK_MAX= 40;

    private ?FightStats $fightStats;

    private string $name;

    /** @var Skill[] */
    private array $skills = [];

    /**
     * @return FightStats
     */
    public function getFightStats(): ?FightStats
    {
        return $this->fightStats;
    }

    /**
     * @param ?FightStats $fightStats
     * @return Beast
     */
    public function setFightStats(?FightStats $fightStats): Beast
    {
        $this->fightStats = $fightStats;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Beast
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Skill[]
     */
    public function getSkills(): array
    {
        return $this->skills;
    }

    /**
     * @param Skill[] $skills
     * @return Beast
     */
    public function setSkills(array $skills): Beast
    {
        $this->skills = $skills;
        return $this;
    }


}