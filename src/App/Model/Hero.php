<?php

namespace App\Model;


use App\Model\Skills\Skill;

class Hero
{
    const NAME = 'Orderus';

    const HEALTH_MIN = 70;
    const HEALTH_MAX= 100;

    const STRENGTH_MIN = 70;
    const STRENGTH_MAX= 80;

    const DEFENCE_MIN = 45;
    const DEFENCE_MAX= 55;

    const SPEED_MIN = 40;
    const SPEED_MAX= 50;

    const LUCK_MIN = 10;
    const LUCK_MAX= 30;

    private FightStats $fightStats;

    /** @var Skill[] */
    private array $skills = [];

    private $name;

    /**
     * @return FightStats
     */
    public function getFightStats(): FightStats
    {
        return $this->fightStats;
    }

    /**
     * @param FightStats $fightStats
     * @return Hero
     */
    public function setFightStats(FightStats $fightStats): Hero
    {
        $this->fightStats = $fightStats;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Hero
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array
     */
    public function getSkills(): array
    {
        return $this->skills;
    }

    /**
     * @param array $skills
     * @return Hero
     */
    public function setSkills(array $skills): Hero
    {
        $this->skills = $skills;
        return $this;
    }
}