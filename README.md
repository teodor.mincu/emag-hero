In order to run the eMAG Hreo Game, please follow the steps:

1. git clone git@gitlab.com:teodor.mincu/emag-hero.git

2. cd emag-hero

3. composer install 

4. php src/App/Command/FightCommand.php

In order to run phpunit tests:

1. alias phpunit="php ./vendor/phpunit/phpunit/phpunit"

2. phpunit