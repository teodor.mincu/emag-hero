<?php


use PHPUnit\Framework\TestCase;
use \App\Model\Beast;

class BeastCreatorTest extends TestCase
{
    /** @var App\Model\Beast */
    protected static $beast;

    public static function setUpBeforeClass(): void
    {
        $beastCreator = new App\Model\Factory\BeastCreator();
        static::$beast = $beastCreator->createFighter();
    }

    public function testBeastHasFightStats()
    {
        $this->assertNotNull(static::$beast->getFightStats());
    }

    /**
     * @depends testBeastHasFightStats
     */
    public function testBeastHasHealthInInterval()
    {
        $this->assertTrue(static::$beast->getFightStats()->getHealth() >= Beast::HEALTH_MIN && static::$beast->getFightStats()->getHealth() <= Beast::HEALTH_MAX);
    }

    /**
     * @depends testBeastHasFightStats
     */
    public function testBeastHasStrengthInInterval()
    {
        $this->assertTrue(static::$beast->getFightStats()->getStrength() >= Beast::STRENGTH_MIN && static::$beast->getFightStats()->getStrength() <= Beast::STRENGTH_MAX);
    }

    /**
     * @depends testBeastHasFightStats
     */
    public function testBeastHasDefenceInInterval()
    {
        $this->assertTrue(static::$beast->getFightStats()->getDefence() >= Beast::DEFENCE_MIN && static::$beast->getFightStats()->getDefence() <= Beast::DEFENCE_MAX);
    }

    /**
     * @depends testBeastHasFightStats
     */
    public function testBeastHasSpeedInInterval()
    {
        $this->assertTrue(static::$beast->getFightStats()->getSpeed() >= Beast::SPEED_MIN && static::$beast->getFightStats()->getSpeed() <= Beast::SPEED_MAX);
    }

    /**
     * @depends testBeastHasFightStats
     */
    public function testBeastHasLuckInInterval()
    {
        $this->assertTrue(static::$beast->getFightStats()->getLuck() >= Beast::LUCK_MIN && static::$beast->getFightStats()->getLuck() <= Beast::LUCK_MAX);
    }

}