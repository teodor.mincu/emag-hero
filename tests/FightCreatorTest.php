<?php

use PHPUnit\Framework\TestCase;
use \App\Model\Hero;
use \App\Model\Beast;

class FightCreatorTest extends TestCase
{
    /** @var App\Model\Fight */
    protected static $fight;

    /** @var Hero */
    protected static $hero;

    /** @var Beast */
    protected static $beast;

    public static function setUpBeforeClass(): void
    {
        $fightCreator = new App\Model\Factory\FightCreator();
        static::$fight = $fightCreator->createFight();

        $heroCreator = new App\Model\Factory\HeroCreator();
        static::$hero = $heroCreator->createFighter();

        $beastCreator = new App\Model\Factory\BeastCreator();
        static::$beast = $beastCreator->createFighter();
    }

    public function testIfTurnsAreSetToOne()
    {
        $this->assertEquals(1, static::$fight->getTurns());
    }
}