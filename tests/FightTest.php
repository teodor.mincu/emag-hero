<?php

namespace Tests;

use App\Model\Beast;
use App\Model\Factory\BeastCreator;
use App\Model\Factory\FightCreator;
use App\Model\Factory\HeroCreator;
use App\Model\Fight;
use App\Model\Hero;
use PHPUnit\Framework\TestCase;

class FightTest extends TestCase
{
    /** @var Fight */
    protected static $fight;

    /** @var Hero */
    protected static $hero;

    /** @var Beast */
    protected static $beast;

    public static function setUpBeforeClass(): void
    {
        $fightCreator = new FightCreator();
        static::$fight = $fightCreator->createFight();

        $heroCreator = new HeroCreator();
        static::$hero = $heroCreator->createFighter();

        $beastCreator = new BeastCreator();
        static::$beast = $beastCreator->createFighter();
    }

    public function testIfHeroIsFirstAttackerBySpeed()
    {
        static::$beast->getFightStats()->setSpeed(40);
        static::$hero->getFightStats()->setSpeed(50);
        static::$fight->determineFirstAttacker(static::$hero, static::$beast);

        $this->assertEquals(static::$hero, static::$fight->getAttacker());
    }

    public function testIfBeastIsFirstAttackerBySpeed()
    {
        static::$beast->getFightStats()->setSpeed(60);
        static::$hero->getFightStats()->setSpeed(50);
        static::$fight->determineFirstAttacker(static::$hero, static::$beast);

        $this->assertEquals(static::$beast, static::$fight->getAttacker());
    }

    public function testIfHeroIsFirstAttackerByLuck()
    {
        static::$beast->getFightStats()->setSpeed(40)->setLuck(25);
        static::$hero->getFightStats()->setSpeed(40)->setLuck(30);
        static::$fight->determineFirstAttacker(static::$hero, static::$beast);

        $this->assertEquals(static::$hero, static::$fight->getAttacker());
    }

    public function testIfBeastIsFirstAttackerByLuck()
    {
        static::$beast->getFightStats()->setSpeed(40)->setLuck(40);
        static::$hero->getFightStats()->setSpeed(40)->setLuck(30);
        static::$fight->determineFirstAttacker(static::$hero, static::$beast);

        $this->assertEquals(static::$beast, static::$fight->getAttacker());
    }


    public function testIfTheGoodIsFirstAttackerInCaseOfSameStats()
    {
        static::$beast->getFightStats()->setSpeed(40)->setLuck(30);
        static::$hero->getFightStats()->setSpeed(40)->setLuck(30);
        static::$fight->determineFirstAttacker(static::$hero, static::$beast);

        $this->assertEquals(static::$hero, static::$fight->getAttacker());
    }


    public function testIfDefenderIsHeroWhenAttackerIsBeast()
    {
        static::$fight->setAttacker(static::$hero);
        static::$fight->determineDefender(static::$hero, static::$beast);
        $this->assertEquals(static::$beast, static::$fight->getDefender());

    }

    public function testIfDefenderIsBeastWhenAttackerIsHero()
    {
        static::$fight->setAttacker(static::$beast);
        static::$fight->determineDefender(static::$hero, static::$beast);
        $this->assertEquals(static::$hero, static::$fight->getDefender());
    }

    public function testIfNoAttackerSet()
    {
        static::$fight->setDefender(null);
        static::$fight->setAttacker(null);
        $this->assertFalse(static::$fight->determineDefender(static::$hero, static::$beast));
    }

    public function testDetermineDamage()
    {
        static::$hero->getFightStats()->setStrength(80);
        static::$beast->getFightStats()->setDefence(60);
        static::$fight
            ->setAttacker(static::$hero)
            ->setDefender(static::$beast);

        $this->assertEquals(20, static::$fight->calculateDamage());
    }

    public function testFightIsFinishedWhenTurnsReachedMaximum()
    {
        static::$fight->setTurns(Fight::MAX_TURNS);
        $this->assertFalse(static::$fight->fightIsOn());
    }

    public function testFightNotReachedMaxTurns()
    {
        static::$fight->setTurns(Fight::MAX_TURNS - 1);
        $this->assertTrue(static::$fight->fightIsOn());
    }

    public function testIfHeroIsLuckyDefender()
    {
        static::$fight->getDefender()->getFightStats()->setLuck(30);
        $testChanceNumber = 7;
        $this->assertTrue(static::$fight->isLuckyDefender($testChanceNumber));
    }

    public function testIfHeroIsNotLuckyDefender()
    {
        static::$fight->getDefender()->getFightStats()->setLuck(30);
        $testChanceNumber = 37;
        $this->assertFalse(static::$fight->isLuckyDefender($testChanceNumber));
    }

    public function testMakeBeastAttackerFromDefender()
    {
        static::$fight->setAttacker(static::$hero);
        static::$fight->setDefender(static::$beast);
        static::$fight->changeFightRoles();
        $this->assertEquals(static::$beast, static::$fight->getAttacker());
    }

    public function testMakeHeroAttacker()
    {
        static::$fight->setAttacker(static::$beast);
        static::$fight->setDefender(static::$hero);
        static::$fight->changeFightRoles();

        $this->assertEquals(static::$hero, static::$fight->getAttacker());
    }
}