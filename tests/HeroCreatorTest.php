<?php

use PHPUnit\Framework\TestCase;
use \App\Model\Hero;

class HeroCreatorTest extends TestCase
{
    /** @var App\Model\Hero */
    protected static $hero;

    public static function setUpBeforeClass(): void
    {
        $heroCreator = new App\Model\Factory\HeroCreator();
        static::$hero = $heroCreator->createFighter();
    }

    public function testHeroHasFightStats()
    {
        $this->assertNotNull(static::$hero->getFightStats());
    }

    public function testHeroHasHealthInInterval()
    {
        $this->assertTrue(static::$hero->getFightStats()->getHealth() >= Hero::HEALTH_MIN && static::$hero->getFightStats()->getHealth() <= Hero::HEALTH_MAX);
    }

    /**
     * @depends testHeroHasFightStats
     */
    public function testHeroHasStrengthInInterval()
    {
        $this->assertTrue(static::$hero->getFightStats()->getStrength() >= Hero::STRENGTH_MIN && static::$hero->getFightStats()->getStrength() <= Hero::STRENGTH_MAX);
    }

    /**
     * @depends testHeroHasFightStats
     */
    public function testHeroHasDefenceInInterval()
    {
        $this->assertTrue(static::$hero->getFightStats()->getDefence() >= Hero::DEFENCE_MIN && static::$hero->getFightStats()->getDefence() <= Hero::DEFENCE_MAX);
    }

    /**
     * @depends testHeroHasFightStats
     */
    public function testHeroHasSpeedInInterval()
    {
        $this->assertTrue(static::$hero->getFightStats()->getSpeed() >= Hero::SPEED_MIN && static::$hero->getFightStats()->getSpeed() <= Hero::SPEED_MAX);
    }

    /**
     * @depends testHeroHasFightStats
     */
    public function testHeroHasLuckInInterval()
    {
        $this->assertTrue(static::$hero->getFightStats()->getLuck() >= Hero::LUCK_MIN && static::$hero->getFightStats()->getLuck() <= Hero::LUCK_MAX);
    }
}